<?php
$items=array(
	"name" => array(
		"desc" => "Имя",
		"type" => "text",
		"maxlength" => "255",
		"size" => "70",
		"select_on_edit" => true,
	),
	"email" => array(
		"desc" => "e-mail",
		"type" => "text",
		"maxlength" => "255",
		"size" => "70",
		"select_on_edit" => true,
		"select_on_edit_disabled" => true,
	),
);
?>