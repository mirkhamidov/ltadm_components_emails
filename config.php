<?php
$config=array(
        "name" => "Адреса e-mail для получения заявок и уведомлений",
        "status" => "system",
        "menu_icon" => "icon-envelope-open",
        "windows" => array(
                "create" => array("width" => 500,"height" => 300),
                "edit" => array("width" => 500,"height" => 500),
                "list" => array("width" => 500,"height" => 400),
        ),
  "list" => array(
    "name" => array("isLink" => true),
    "email" => array("align" => "center"),
  ),
        "right" => array("admin","#GRANTED"),
        "main_table" => "lt_emails",
        "select" => array(
                "max_perpage" => 20,
                "default_orders" => array(
                ),
                "default" => array(
                ),
        ),
);

$actions=array(
        "create" => array(
                "before_code" => "",
        ),
        "edit" => array(
                "before_code" => "",
        ),
);
?>
